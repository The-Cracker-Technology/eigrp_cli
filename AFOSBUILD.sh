rm -rf /opt/ANDRAX/eigrp_cli

source /opt/ANDRAX/PYENV/python2/bin/activate

/opt/ANDRAX/PYENV/python2/bin/pip2 install pypcap dpkt

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Pip install... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf andraxbin/* /opt/ANDRAX/bin
rm -rf andraxbin

cp -Rf $(pwd) /opt/ANDRAX/eigrp_cli

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
